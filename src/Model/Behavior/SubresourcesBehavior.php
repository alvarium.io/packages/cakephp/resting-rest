<?php
namespace Alvarium\RestingRest\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;

/**
 * Subresources behavior
 */
class SubresourcesBehavior extends Behavior
{

    /**
     * Finder for subresources
     *
     * @param \Cake\ORM\Query $query Query instance.
     * @param \array $options Array instance.
     * @return \Cake\ORM\Query
     */
    public function findSubresources(Query $query, array $options)
    {
        $associations = $this->_table->associations()->getByType('belongsToMany');

        foreach ($options as $key => $value) {
            foreach ($associations as $index => $association) {
                if ($association->getTargetForeignKey() === $key) {
                    return $query->innerJoinWith($association->getName(), function ($query) use ($key, $value) {
                        return $query->where([
                            $key => $value,
                        ]);
                    });
                }
            }
            if ($this->_table->hasField($key)) {
                $query->where([$key => $value]);
            }
        }

        return $query;
    }
}