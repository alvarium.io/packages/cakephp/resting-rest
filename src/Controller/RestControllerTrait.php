<?php
namespace Alvarium\RestingRest\Controller;

use Cake\Utility\Inflector;

trait RestControllerTrait
{
    public $rest = [
        'finders' => [],
        'options' => [
            'edit' => [
                'get' => [],
                'patch' => [],
            ],
        ],
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Alvarium/RestingRest.RestPaginator');
    }

    protected function getLocalVariables()
    {
        $lcfirst = lcfirst($this->modelClass);

        return [
            $this->modelClass,
            Inflector::singularize($lcfirst),
            $lcfirst,
        ];
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        list($model, $varname) = $this->getLocalVariables();

        $$varname = $this->$model->newEntity();
        if ($this->request->is('post')) {
            $options = [];
            if (!empty($this->rest['options']['add'])) {
                $options = $this->rest['options']['add'];
            }
            $$varname = $this->$model->patchEntity($$varname, $this->request->getData(), $options);
            if ($this->$model->save($$varname)) {
                if (!$this->RequestHandler->prefers('json')) {
                    $this->Flash->success(__('The entity has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->response = $this->response->withStatus('201');
                }
            } else {
                if (!$this->RequestHandler->prefers('json')) {
                    $this->Flash->error(__('The entity could not be saved. Please, try again.'));
                }
                $errors = $$varname->errors();
                $this->response = $this->response->withStatus('400');
                $this->set(compact('errors'));
            }
        }

        $this->set(compact($varname));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        list($model, , $varname) = $this->getLocalVariables();

        $find = $this->$model;

        if (!empty($this->rest['finders']['index'])) {
            $find = $this->rest['finders']['index'];
        }

        $this->set([
            $varname => $this->RestPaginator->paginate($find, $this->paginate)
        ]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Entity id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        list($model, $varname) = $this->getLocalVariables();

        $this->autoRender = false;

        $this->request->allowMethod(['post', 'delete']);
        $$varname = $this->$model->get($id);
        if ($this->$model->delete($$varname)) {
            if (!$this->RequestHandler->prefers('json')) {
                $this->Flash->success(__('Entity has been deleted.'));
            } else {
                $this->response = $this->response->withStatus('204');
            }
        } else {
            if (!$this->RequestHandler->prefers('json')) {
                $this->Flash->error(__('Entity could not be deleted. Please, try again.'));
            }
            $errors = $$varname->errors();
            $this->response = $this->response->withStatus('400');
            $this->set(compact('errors'));
        }
        if (!$this->RequestHandler->prefers('json')) {
            return $this->redirect(['action' => 'index']);
        }
    }


    /**
     * Edit method
     *
     * @param string|null $id Entity id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        list($model, $varname) = $this->getLocalVariables();

        $options = [];
        if (!empty($this->rest['options']['edit']['get'])) {
            $options = $this->rest['options']['edit']['get'];
        }

        $$varname = $this->$model->get($id, $options);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $options = [];
            if (!empty($this->rest['options']['edit']['patch'])) {
                $options = $this->rest['options']['edit']['patch'];
            }

            $$varname = $this->$model->patchEntity($$varname, $this->request->getData(), $options);
            $this->set(compact($varname));
            if ($this->$model->save($$varname)) {
                if (!$this->RequestHandler->prefers('json')) {
                    $this->Flash->success(__('The entity has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->response = $this->response->withStatus('201');
                }
            } else {
                if (!$this->RequestHandler->prefers('json')) {
                    $this->Flash->error(__('The entity could not be saved. Please, try again.'));
                }
                $errors = $$varname->errors();
                $this->response = $this->response->withStatus('400');
                $this->set(compact('errors'));
            }
        }
    }

    /**
     * View method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        list($model, $varname) = $this->getLocalVariables();

        $options = [];
        if (!empty($this->rest['options']['view'])) {
            $options = $this->rest['options']['view'];
        }
        if (empty($options['conditions'])) {
            $options['conditions'] = ["$model.id" => $id];
        } else {
            $options['conditions'] += ["$model.id" => $id];
        }

        $$varname = $this->$model->find('all', $options)->first();

        if (!empty($this->rest['finders']['view'])) {
            $$varname = $this->rest['finders']['view'];
        }

        $this->set(compact($varname));
    }
}
