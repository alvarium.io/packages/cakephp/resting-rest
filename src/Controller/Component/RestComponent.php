<?php
namespace Alvarium\RestingRest\Controller\Component;

use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\Controller\Component;

class RestComponent extends Component
{
    public function beforeRender(Event $event)
    {
        $controller = $event->getSubject();
        if (isset($controller->viewVars['_serialize'])) {
            return;
        }

        $varname = lcfirst($controller->modelClass);
        if ($controller->request->getParam('action') != 'index') {
            $varname = Inflector::singularize($varname);
        }


        if (array_key_exists($varname, $controller->viewVars)) {
            if (gettype($controller->viewVars[$varname]) === 'string') {
                return;
            }
            if (
                $controller->request->getParam('action') !== 'index' &&
                $controller->viewVars[$varname] &&
                method_exists($controller->viewVars[$varname], 'hasErrors') &&
                $controller->viewVars[$varname]->hasErrors()
            ) {
                $controller->viewVars['_serialize'] = 'errors';
            } else {
                $controller->viewVars['_serialize'] = $varname;
            }
        }
    }
}
