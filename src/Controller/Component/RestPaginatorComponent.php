<?php
namespace Alvarium\RestingRest\Controller\Component;

use Cake\Controller\Component\PaginatorComponent;
use Cake\ORM\Query;

class RestPaginatorComponent extends PaginatorComponent
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    protected function _setPagingParams()
    {
        parent::_setPagingParams();

        $controller = $this->getController();
        $response = $controller->getResponse();
        $table = $controller->modelClass;

        $paging = $this->_paginator->getPagingParams()[$table];
        $response = $response->withHeader('X-Total-Pages', $paging['pageCount']);
        $controller->setResponse($response);
    }
}
